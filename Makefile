################################################################################
# File:		Makefile
# Description:	Makefile to edit and prepare .scad files for 3D printing
# Maintainer:	Léo DALECKI
################################################################################

################################################################################
# Environnement
################################################################################

# OpenSCAD options when previewing model
PREVIEWFLAGS := -D 'debug=false'

# OpenSCAD options when editing model
EDITFLAGS := -D 'debug=true'

# OpenSCAD options when rendering model
RENDERFLAGS := -D 'debug=false'

-include envvars

################################################################################
# Variables
################################################################################

SCADFILES := $(wildcard */*.scad)
STLFILES := $(foreach scad,$(SCADFILES),$(subst .scad,.stl,$(scad)))
FILE := $(firstword $(MAKECMDGOALS))

SHELL := $(shell which bash)

################################################################################
# Targets
################################################################################

.ONESHELL:
.PHONY: all preview edit mrproper $(SCADFILES) 

all: $(STLFILES) # Render all .scad models to .stl

%.stl: %.scad # Render .scad model to .stl
	openscad -o $@ $(RENDERFLAGS) $<

preview: # `make <scad> preview` opens preview of .scad file <scad>
	@openscad $(PREVIEWFLAGS) $(FILE)

edit: # `make <scad> edit` setups editing environnement for .scad file <scad>
	-@openscad $(EDITFLAGS) $(FILE) >/dev/null 2>&1 &
	openscad_pid=$$!
	$(VISUAL) $(FILE)
	kill $$openscad_pid >/dev/null 2>&1

mrproper: # Wipe .stl files
	rm -f $(STLFILES)
