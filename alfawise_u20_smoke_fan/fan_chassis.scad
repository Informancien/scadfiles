
fan_diam		= 120;	// Fan's diameter
fan_thickness		= 25;	// Fan's thickness
fan_wall		= 2.5;	// Thickness of fan's walls

filter_thickness	= 20;	// Filter's thickness

screw_margin		= 7.5;	// Space between fan's edge and middle of screw

profile_width		= 20.8;	// Width of metal profile
profile_slit		= 5;	// Height of profile's slit

$fn = 100;

thickness = filter_thickness + fan_wall * 2;
width = fan_diam + fan_wall * 2;

difference() {
	cube([width, width, thickness],
		center = true);

	for(i = [1 : 4]) {
		rotate([0, 0, 360 / 4 * i + 45]) 
		translate([( fan_diam * sqrt(2) ) / 2 - ( screw_margin
			* sqrt(2) ), 0])
			cylinder(h = thickness + 1, d = 4, center = true);
	}

	cylinder(h = thickness + 1, d = fan_diam - fan_wall * 2,
		center = true);

	cube([fan_diam, fan_diam, thickness - fan_wall * 2],
		center = true);
}

translate([0, 0, ( thickness - profile_width - fan_wall ) / 2]) {
	translate([( width - profile_slit ) / 2 + profile_width, 0,
		-fan_wall / 2])
		cube([profile_slit, width, profile_slit],
			center = true);

	difference() {
		translate([(width + profile_width + fan_wall ) / 2, 0, 0])
			cube([profile_width + fan_wall, width, profile_width
				+ fan_wall], center = true);

		translate([(width + profile_width ) / 2, 0, -fan_wall / 2 - .5])
			cube([profile_width, width + 1, profile_width + 1],
				center = true);
	}
}
