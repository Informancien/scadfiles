
slices		= 5;	// Number of stacked cylinders

slice_height	= 20;	// Height of first cylinder
slice_diam	= 70;	// Diameter of first slice

diam_factor	= .75;	// Factor of cylinder's diameter from to previous one

for(i = [1 : slices]) {

	divider = i == 1 ? 1 : i - 1;
	factor = i == 1 ? 1 : diam_factor / divider;

	diam = slice_diam * factor;

	translate([0, 0, slice_height * ( i - 1 )])
		cylinder(d = diam, h = slice_height);
}
