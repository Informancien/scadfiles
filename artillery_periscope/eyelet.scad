/*******************************************************************************
File: eyelet.scad
Description: Replacement eyelet for french army OPL periscope, to be 3D printed
 in flexible TPU. Designed to be printed without supports.
Maintainer: Léo DALECKI
*******************************************************************************/

/*******************************************************************************
Parameters
*******************************************************************************/

debug 		= false;	// Debugging, avoid lenghty rendering

eyelet_diam	= 36;	// Diameter of the metal ring retaining the eyelet

cone_height	= 15;	// Height of eyelet's "cone"

lip_height	= 2;	// Height of the eyelet's lip inserted in the metal ring
lip_indents	= 10;	// Number of indents in retaining lip

ring_thickness	= 10;	// Thickness of the outer ring
ring_opening	= 26;	// Diameter of outer ring's opening

thickness	= 2;	// Minimum wall's thickness

fn		= 100;	// Face number when rendering

/*******************************************************************************
Model
*******************************************************************************/

$fn = debug ? $fn : fn;

if (debug)
	echo("\nDEBUGGING\n");

difference() {
	union() {
		// Lip
		cylinder(r = eyelet_diam / 2, h = lip_height);

		// Outer wall
		translate([0, 0, lip_height])
			cylinder(r1 = eyelet_diam / 2,
				r2 = ring_opening / 2 + ring_thickness,
				h = cone_height - ring_thickness / 2);
	}

	// Inner wall
	translate([0, 0, lip_height])
		cylinder(r1 = eyelet_diam / 2 - thickness,
			r2 = ring_opening / 2,
			h = cone_height - ring_thickness / 2);

	// Ring opening
	translate([0, 0, -1])
		cylinder(r = ring_opening / 2,
			h = lip_height + cone_height + 2);

	// Lip indents
	for(i = [1:1:lip_indents])
		rotate([0, 0, 360 / lip_indents * i])
		translate([eyelet_diam / 2 - 7.5, 0, 1])
			resize([10, 15, lip_height + 2])
			rotate([0, 0, 45])
			cube([10, 10, lip_height + 2], center = true);
}

// Outer ring
translate([0, 0, cone_height + lip_height - ring_thickness / 2])
rotate_extrude()
translate([(ring_opening + ring_thickness) / 2, 0])
	circle(r = ring_thickness / 2);
