/*******************************************************************************
File: simple_screw_thread.scad 
Description: Modules for generating screw threads from child shape (in an overly
 simplistic way).
Maintainer: Léo DALECKI
License: GNU Lesser Genaral Public License
 Copyright 2021 Léo DALECKI

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation, either version 3 of the License, or (at your option) any
 later version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 details.

 You should have received a copy of the GNU Lesser General Public License along
 with this program. If not, see <https://www.gnu.org/licenses/>.
Usage: use <screw_thread.scad>
 
 screw_thread(lead = <lead>, height = <height>, diameter = <diameter>,
 	[starts = <starts>][, left_handed = true])
 	<3d_shape>;
 
 screw_thread_2d(lead = <lead>, height = <height>, diameter = <diameter>,
 	[starts = <starts>][, left_handed = true])
 	<2d_shape>;
 
 Generate screw thread from a 3D or 2D shape, see modules for detailed
 parameters description.
*******************************************************************************/

/*******************************************************************************
Modules 
*******************************************************************************/

module screw_thread(lead, height, diameter, starts = 1, left_handed = false) {
/*******************************************************************************
Description: Generate a screw thread using chained hull.
Usage:
 screw_thread(lead = <lead>, height = <height>, diameter = <diameter>[,
 	starts = <starts>][, left_handed = true][, $fa = <fa>])
 	<3d_shape>;
 
 Makes a screw thread using child <3d_shape> of lead <lead>, height <height>,
 diameter <diameter>, optionally more that one start if <starts> is given and
 left handed if left_handed is set to true.
Parameters:
 lead		Float height of the lead;
 height		Float approx. total height of threading;
 diameter	Float diameter of threading;
 starts		Optional integer of starts, 1 by default;
 left_handed	Optional boolean to set thread to be left handed;
 $fa		Optional number of shapes per turn.
*******************************************************************************/

	loops = height / lead;
	angle_per_start = 360 / starts;
	handedness_factor = left_handed == true ? -1 : 1;
	shape_per_loop = 360 / $fa;

	for(i = [1 : starts])
	for(j = [0 : loops - 1]) {

		loop_height = lead * j;

		for(k = [0 : shape_per_loop - 1]) {
			hull() {
				rotate([0, 0,
					$fa * handedness_factor * k
					+ angle_per_start * i])
				translate([diameter / 2, 0,
					loop_height + lead / shape_per_loop
					* k])
					children();

				rotate([0, 0,
					$fa * handedness_factor * (k + 1)
					+ angle_per_start * i])
				translate([diameter / 2, 0,
					loop_height + lead / shape_per_loop
					* (k + 1)])
					children();
			}
		}
	}
}

module screw_thread_2d(lead, height, diameter, starts = 1,
	left_handed = false) {
/*******************************************************************************
Description: Generate a screw thread from a 2D shape.
Usage:
 screw_thread_2d(lead = <lead>, height = <height>, diameter = <diameter>[,
 	starts = <starts>][, left_handed = true][, $fa = <fa>])
 	<2d_shape>;
 
 Makes a screw thread using 2D shape child <2d_shape>, see module screw_thread
 for description of parameters.
Parameters:
 lead		Float height of the lead;
 height		Float approx. total height of threading;
 diameter	Float diameter of threading;
 starts		Optional integer of starts, 1 by default;
 left_handed	Optional boolean to set thread to be left handed;
 $fa		Optional number of shapes per turn.
*******************************************************************************/

	screw_thread(lead, height, diameter, starts, left_handed)
		rotate([90, 0, 0])
		linear_extrude(10 ^ -6) // Extrude by a micron to allow 3D hull
			children();
}

/*******************************************************************************
Tests 
*******************************************************************************/

cylinder(d = 10, h = 30); // Shaft
#cylinder(d = 10 + 1, h = 5); // Lead

// Test thread
screw_thread_2d(lead = 5, height = 30, diameter = 10, starts = 3,
	left_handed = true, $fa = 22.5)
	circle(d = 1, $fn = 3);
